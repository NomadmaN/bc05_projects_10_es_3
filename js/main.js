const todoList = [];
const completeList = [];

renderToDoList();
renderCompletedList();

// function render
function renderToDoList(){
    let contentList = '';
    todoList.forEach((item, index) => {
        contentList += `
        <li>${item}
            <div class="buttons">
                <button>
                    <i onclick="removeItem(${index})" class="fa fa-trash-alt remove" title="Remove this task"></i>
                    <i onclick="completedItem(${index})" class="fa fa-check-circle complete" title="Check this task as completed"></i>
                </button>
            </div>
        </li>
        `;
    })
    document.getElementById('todo').innerHTML = contentList;
}

function renderCompletedList(){
    let contentList = '';
    completeList.forEach((item, index) => {
        contentList += `
        <li><span>${item}</span>
            <div class="buttons">
                <button>
                        <i onclick="removeItemCompleted(${index})" class="remove fas fa-trash-alt" title="Remove this task"></i>
                </button>
                <button class="complete">
                        <i onclick="completedItem(${index})" class="fas fa-check-circle" title="This task is completed"></i>
                </button>
            </div>
        </li>
        `;
    })
    document.getElementById('completed').innerHTML = contentList;
}

// function add new task
function addToList(){
    let taskInput = document.getElementById('newTask').value;
    todoList.push(taskInput)
    renderToDoList();
}

// function remove task
function removeItem(index){
    todoList.forEach((item) => {
        let i = todoList.indexOf(item)
        if (i === index){
            todoList.splice(index,1)
        }
    })
    renderToDoList();
}

function removeItemCompleted(index){
    completeList.forEach((item) => {
        let i = completeList.indexOf(item)
        if (i === index){
            completeList.splice(index,1)
        }
    })
    renderCompletedList();
}

// function complete task
function completedItem(index){
    todoList.forEach((item) => {
        let i = todoList.indexOf(item)
        if (i === index){
            completeList.push(item)
            renderCompletedList();
            todoList.splice(index,1)
            renderToDoList();
        }
    })   
}

// sorting function
function sortAZ(){
    todoList.sort()
    renderToDoList();
}

function sortZA(){
    todoList.reverse()
    renderToDoList();
}